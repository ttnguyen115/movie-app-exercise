import { Paper, TableContainer, TableRow, TableBody, TableCell, TableHead, Table, Typography } from '@material-ui/core';
import React from 'react';
import OrderRow from '../OrderRow';
import { data } from '../../constants';

const OrderTable = ({ thongTinDatVe }) => {
    const orderInfo = data;

    return (
        <>
            <TableContainer component={Paper} style={{ marginTop: 50 }}>
                <Typography variant="h6" component="h6">Order List</Typography>
                <Table aria-label="collapsible table">
                    <TableHead>
                        <TableRow>
                            <TableCell />
                            <TableCell>Ticket ID</TableCell>
                            <TableCell align="left">Movie Name</TableCell>
                            <TableCell align="left">Duration</TableCell>
                            <TableCell align="right">Price</TableCell>
                            <TableCell align="right">Ordered Date</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {
                            orderInfo.map(order => (
                                <OrderRow key={order.maVe} order={order} />
                            ))
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    )
}

export default OrderTable
