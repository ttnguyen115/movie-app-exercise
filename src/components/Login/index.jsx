import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import * as yup from "yup";
import { login } from '../../store/userSlice';
import InputField from '../FormControl/InputField';
import PasswordField from '../FormControl/PasswordField';
import { ContainerStyled } from './styles';

const schema = yup.object().shape({
    username: yup.string()
        .required('Please enter your username.'),
    password: yup.string()
        .required('Please enter your password.')
        .min(6,'Please enter at least six characters.'),
});

const Login = ({ closeDialog }) => {
    const dispatch = useDispatch();
    const { control, handleSubmit, formState: { isSubmitting } } = useForm({
        defaultValues: {
            username: 'trungnguyen',
            password: 'Thanhtrung',
        },
        reValidateMode: 'onSubmit',
        resolver: yupResolver(schema)
    });
    const onSubmit = data => {
        dispatch(login(data));
        closeDialog();
    }

    return (
        <>
            <ContainerStyled maxWidth="sm">
                <Typography variant="h5" component="h3">Log in</Typography>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <InputField control={control} name="username" label="Username" value="trungnguyen" />
                    <PasswordField control={control} name="password" label="Password" value="Thanhtrung" />
                    <Button
                        disabled={isSubmitting}
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        size="large"
                    >
                        Log in
                    </Button>
                </form>
            </ContainerStyled>
        </>
    )
}

export default Login
