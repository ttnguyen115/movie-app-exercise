import { AppBar, Box, Button, Dialog, DialogContent, Typography } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { logout } from '../../store/userSlice';
import Login from '../Login';
import Register from '../Register';
import { CloseBtn, ControlStyled, IconBtn, NavLinkStyled, ToolbarStyled } from './styles';

const MODE = {
    REGISTER: 'REGISTER',
    LOGIN: 'LOGIN'
}

const Header = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { token } = useSelector(state => state.user);
    const [open, setOpen] = useState(false);
    const [mode, setMode] = useState(MODE.LOGIN);
    const username = localStorage.getItem('username');

    const handleClickOpen = e => {
        setMode(e.target.innerText);
        setOpen(true);
    }
    const handleClose = () => setOpen(false);

    const handleLogout = () => {
        dispatch(logout());
        history.push("/");
    }

    return (
        <>
            <AppBar position="static">
                <ToolbarStyled>
                    <Typography variant="h6" className="">
                        <NavLinkStyled to="/">Movie App</NavLinkStyled>
                    </Typography>

                    <ControlStyled>
                        {
                            token 
                            ? <>
                                <Typography style={{ marginRight: 10 }}>Hello, {username}</Typography>
                                <NavLinkStyled to={`/user/${username}`}>
                                    <IconBtn><AccountCircleOutlinedIcon/></IconBtn>
                                </NavLinkStyled>
                                <IconBtn onClick={handleLogout}><ExitToAppIcon /></IconBtn>
                            </> 
                            : <>
                                <IconBtn onClick={handleClickOpen}>Login</IconBtn>
                                <IconBtn onClick={handleClickOpen}>Register</IconBtn>
                            </>
                        }
                    </ControlStyled>
                </ToolbarStyled>
            </AppBar>
        
            <Dialog 
                disableBackdropClick 
                disableEscapeKeyDown 
                open={open} 
                onClose={handleClose} 
                aria-labelledby="form-dialog-title"
            >
                <CloseBtn onClick={handleClose}><Close /></CloseBtn>
                <DialogContent>
                    {mode === MODE.REGISTER && (
                        <>
                            <Register closeDialog={handleClose}></Register>
                            <Box textAlign="center">
                                <Button color="primary" onClick={() => setMode(MODE.LOGIN)}>
                                    Already an account. Login here 
                                </Button>
                            </Box>
                        </>
                    )}
                    {mode === MODE.LOGIN && (
                        <>
                            <Login closeDialog={handleClose}></Login>
                            <Box textAlign="center">
                                <Button color="primary" onClick={() => setMode(MODE.REGISTER)}>
                                    Don't have an account. Register here 
                                </Button>
                            </Box>
                        </>
                    )}
                </DialogContent>
            </Dialog>
        </>
    )
}

export default Header
