import { Button, IconButton, Toolbar } from "@material-ui/core";
import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const ToolbarStyled = styled(Toolbar)`
    justify-content: space-between;
`;

export const NavLinkStyled = styled(NavLink)`
    text-decoration: none;
    margin: 0 15px;
    color: white;
    font-weight: 500;

    &.${props => props.activeClassName} {
        border-bottom: 2px solid #fff;
    }
`;

export const ControlStyled = styled.div`
    display: flex;
    align-items: center;
`;

export const IconBtn = styled(Button)`
    color: white !important;
    min-width: 0;
`;

export const CloseBtn = styled(IconButton)`
    position: absolute;
    top: 10px;
    right: 10px;
    color: 'grey';
    z-index: 1;
`;

