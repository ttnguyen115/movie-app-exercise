import styled from "styled-components";
import { Container } from '@material-ui/core';

export const ContainerStyled = styled(Container)`
    margin-top: 30px;
`;