import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Typography } from '@material-ui/core';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';
import { register } from '../../store/userSlice';
import InputField from '../FormControl/InputField';
import PasswordField from '../FormControl/PasswordField';
import { ContainerStyled } from './styles';

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const schema = yup.object().shape({
    username: yup.string()
        .required('Please enter your username.'),
    email: yup.string()
        .required('Please enter your email.')
        .email('Please enter an valid email address.'),
    fullname: yup.string()
        .required('Please enter your full name.')
        .test('Should has at least two words.', 'Please enter at least two words.', value => {
            return value.split(' ').length >= 2
        }),
    phone: yup.string()
        .required('Please enter your phone.')
        .matches(phoneRegExp , 'Phone number is not valid'),
    password: yup.string()
        .required('Please enter your password.')
        .min(6,'Please enter at least six characters.'),
    cfPassword: yup.string()
        .required('Please retype your password.')
        .oneOf([yup.ref('password')],'Password does not match.'),
});

const Register = ({ closeDialog }) => {
    const dispatch = useDispatch();
    const { control, handleSubmit, formState: { isSubmitting } } = useForm({
        defaultValues: {
            username: '',
            email: '',
            fullname: '',
            phone: '',
            password: '',
            cfPassword: '',
        },
        reValidateMode: 'onSubmit',
        resolver: yupResolver(schema)
    });
    const onSubmit = data => {
        dispatch(register(data));
        closeDialog();
    }

    return (
        <>
            <ContainerStyled maxWidth="sm">
                <Typography variant="h5" component="h3">Register</Typography>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <InputField control={control} name="username" label="Username" />
                    <InputField control={control} name="email" label="Email" />
                    <InputField control={control} name="fullname" label="Full Name" />
                    <InputField control={control} name="phone" label="phone" />
                    <PasswordField control={control} name="password" label="Password" />
                    <PasswordField control={control} name="cfPassword" label="Confirm Password" />
                    <Button
                        disabled={isSubmitting}
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        size="large"
                    >
                        Register
                    </Button>
                </form>
            </ContainerStyled>
        </>
    )
}

export default Register
