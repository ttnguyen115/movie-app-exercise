import { Box, Collapse, IconButton, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import moment from 'moment';
import React, { useState } from 'react';

const OrderRow = ({ order }) => {
    const [open, setOpen] = useState(false);
    const handleCollapseTable = () => setOpen(!open);

    return (
        <>
            <TableRow>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={handleCollapseTable}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>

                <TableCell component="th" scope="row">{order.maVe}</TableCell>
                <TableCell align="left">{order.tenPhim}</TableCell>
                <TableCell align="left">{order.thoiLuongPhim} minutes</TableCell>
                <TableCell align="right">{order.giaVe}</TableCell>
                <TableCell align="right">{moment(order.ngayDat).format("DD/MM/YYYY")}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <Box margin={1}>
                        <Table size="small" aria-label="purchases">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Seat ID</TableCell>
                                    <TableCell>Seat Number</TableCell>
                                    <TableCell align="right">Theater Number</TableCell>
                                    <TableCell align="right">Theater</TableCell>
                                </TableRow>
                            </TableHead>

                            <TableBody>
                                {
                                    order.danhSachGhe.map(item => (
                                        <TableRow key={order.maVe}>
                                            <TableCell component="th" scope="row">
                                                {item.maGhe}
                                            </TableCell>
                                            <TableCell>{item.tenGhe}</TableCell>
                                            <TableCell align="right">{item.maCumRap}</TableCell>
                                            <TableCell align="right">{item.tenHeThongRap}</TableCell>
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </Box>
                </Collapse>
                </TableCell>
            </TableRow>
        </>
    )
}

export default OrderRow
