import { Card, CardContent, Container, Grid } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import React from 'react';

const SkeletonList = () => {
    return (
        <>
            <Container maxWidth="lg" style={{ marginTop: 50 }}>
                <Grid container spacing={1}>
                    {
                        Array.from(new Array(20)).map((x, index) => (
                            <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
                                <Card>
                                    <Skeleton animation="wave" variant="rect" height={300} />
                                    <CardContent>
                                        <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                                        <Skeleton animation="wave" height={10} width="80%" />
                                    </CardContent>
                                </Card>
                            </Grid>
                        ))
                    }
                </Grid>
            </Container>
        </>
    )
}

export default SkeletonList
