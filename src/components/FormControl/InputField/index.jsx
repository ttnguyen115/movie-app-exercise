import { TextField } from '@material-ui/core';
import React from 'react';
import { Controller } from 'react-hook-form';

const InputField = ({ control, name, label }) => {
    return (
        <Controller
            name={name}
            control={control}
            render={({ 
                field: { onChange, onBlur, value, name }, 
                fieldState: { invalid, error } 
            }) => (
                <TextField
                    margin="normal"
                    variant="outlined"
                    fullWidth
                    label={label}
                    error={invalid}
                    helperText={error?.message}
                    onChange={onChange}
                    onBlur={onBlur}
                    name={name}
                    value={value}
                />
            )}
        />
    )
}

export default InputField
