import { CardMedia } from "@material-ui/core";
import styled from "styled-components";

export const CardMediaStyled = styled(CardMedia)`
    min-height: 300px;
    height: 100%;
`;