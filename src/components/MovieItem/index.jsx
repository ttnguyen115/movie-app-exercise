import { Button, Card, CardActions, CardContent, Typography } from '@material-ui/core';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { CardMediaStyled } from './styles';

const MovieItem = ({ movie }) => {
    const history = useHistory();
    const handleDetailPage = url => history.push(url);

    return (
        <Card>
            <CardMediaStyled
                image={movie.hinhAnh}
                title={movie.biDanh}
            />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">{movie.tenPhim}</Typography>
                    <Typography variant="body2" color="textSecondary" component="p">{ movie.moTa.substr(0, 50) + '...' }</Typography>
                </CardContent>

            <CardActions>
                {/* <Button size="small" color="primary">Trailer</Button> */}
                <Button size="small" color="primary" onClick={() => handleDetailPage(`/movie/${movie.maPhim}`)}>
                    Details
                </Button>
            </CardActions>
        </Card>
    )
}

export default MovieItem
