import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import userApi from '../../api/userApi';

const initialState = {
    token: "",
    user: null
}

export const login = createAsyncThunk(
    'user/login',
    async (data) => {
        try {
            const { accessToken, taiKhoan } = await userApi.login(data);
            localStorage.setItem('token', accessToken);
            localStorage.setItem('username', taiKhoan);
            return accessToken;
        } catch (err) {
            console.log(err);
        }
    }
)

export const register = createAsyncThunk(
    'user/register',
    async (data) => {
        try {
            const { taiKhoan, matKhau } = await userApi.register(data);
            const { accessToken, taiKhoan: username } = await userApi.login({ taiKhoan, matKhau });
            localStorage.setItem('token', accessToken);
            localStorage.setItem('username', username);
            return accessToken;
        } catch (err) {
            console.log(err);
        }
    }
)

export const fetchUserByToken = createAsyncThunk(
    'user/fetchUserByToken',
    async (token) => {
        try {
            const res = await userApi.getUserByToken(token);
            return res;
        } catch (err) {
            console.log(err);
        }
    }
) 

export const refreshToken = createAsyncThunk(
    'user/refreshToken',
    async () => {
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');

        if (token && username) {
            try {
                const user = await userApi.getUserByToken(token);
                const { accessToken } = await userApi.refresh({ taiKhoan: user.taiKhoan, matKhau: user.matKhau });
                localStorage.setItem('token', accessToken);
                return accessToken;
            } catch (err) {
                console.log(err);
            }
        }
    }
)

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        logout: (state) => {
            state.token = "";
            state.user = null;
            localStorage.removeItem('token');
            localStorage.removeItem('username');
        }
    },
    extraReducers: (builder) => {
        builder
            // Login
            .addCase(login.fulfilled, (state, { payload }) => {
                state.token = payload || "";
            })
            // Register
            .addCase(register.fulfilled, (state, { payload }) => {
                state.token = payload || "";
            })
            // Refresh Token
            .addCase(refreshToken.fulfilled, (state, { payload }) => {
                state.token = payload || "";
            })
            // User data
            .addCase(fetchUserByToken.fulfilled, (state, { payload }) => {
                state.user = payload;
            })
    }
});

export const { logout } = userSlice.actions;

export const user = state => state;

export default userSlice.reducer;