import { Container, Typography } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import styled from 'styled-components';

export const ContainerStyled = styled(Container)`
    min-height: calc(100vh - 64px);
    height: auto;
    padding: 20px;
`;

export const SkeletonContainer = styled.div`
    padding: 20px;
`;

export const SkeletonStyled = styled(Skeleton)`
    margin: 20px auto;
`;

export const ImageStyled = styled.img`
    display: flex;
    justify-content: center;
    margin: 20px auto;
    border-radius: 10px;
`;

export const TypographyStyled = styled(Typography)`
    margin: 10px 0;
`;