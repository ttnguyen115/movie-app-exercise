import { CardContent, Paper } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import movieApi from '../../api/movieApi';
import Layout from '../../HOCs/Layout';
import { ContainerStyled, ImageStyled, SkeletonContainer, SkeletonStyled, TypographyStyled } from './styles';

const Details = () => {
    const { id } = useParams();
    const [loading, setLoading] = useState(true);
    const [movie, setMovie] = useState(null);

    useEffect(() => {
        const fetchMovieById = async id => {
            try {
                const res = await movieApi.get(id);
                setMovie(res);
                setLoading(false);
            } catch (err) {
                console.log(err)
            }
        }

        fetchMovieById(id);
    }, [id]);

    return (
        <Layout>
            <ContainerStyled maxWidth="md">
                {
                    loading 
                    ? <Paper style={{ height: "100vh" }}>
                        <SkeletonContainer>
                            <SkeletonStyled variant="rect" width={640} height={430} />
                            <Skeleton animation="wave" />
                            <Skeleton animation="wave" />
                            <Skeleton animation="wave" width="80%" />
                        </SkeletonContainer>
                    </Paper>
                    : <>
                        <Paper>
                            <CardContent>
                                <ImageStyled src={movie.hinhAnh} alt="" />
                                    <TypographyStyled gutterBottom variant="h5" component="h2">{movie.tenPhim}</TypographyStyled>
                                    <TypographyStyled variant="body2" color="textSecondary" component="p">{movie.moTa}</TypographyStyled>
                                    <TypographyStyled variant="h6" color="textSecondary" component="h6">Release: {moment(movie.ngayKhoiChieu).format("DD/MM/YYYY")}</TypographyStyled>
                                    <TypographyStyled variant="h6" color="textSecondary" component="h6">Rating: {movie.danhGia} / 10</TypographyStyled>
                            </CardContent>
                        </Paper>
                    </> 
                }
            </ContainerStyled>
        </Layout>
    )
}

export default Details
