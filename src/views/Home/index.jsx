import { Container, Grid } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import movieApi from '../../api/movieApi';
import MovieItem from '../../components/MovieItem';
import SkeletonList from '../../components/SkeletonList';
import Layout from '../../HOCs/Layout';
import { PaginationStyled } from './styles';

const Home = () => {
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const [movieList, setMovieList] = useState([]);
    const [loading, setLoading] = useState(true);
    
    useEffect(() => {
        const fetchMovieData = async (page) => {
            try {
                const { totalPages, items } = await movieApi.getAll(page);
                setTotalPages(totalPages);
                setMovieList(items);
                setLoading(false);
            } catch (err) {
                console.log(err);
            }
        } 
        fetchMovieData(page);
    }, [page]);

    const handlePageChange = (e, page) => setPage(page);

    return (
        <Layout>
            {
                loading
                ? <SkeletonList />
                : <Container maxWidth="lg" style={{ marginTop: 50 }}>
                    <Grid container spacing={1}>
                        {
                            movieList.map(movie => (
                                <Grid item xs={12} sm={6} md={4} lg={3} key={movie.maPhim}>
                                    <MovieItem movie={movie} />
                                </Grid>
                            ))
                        }
                    </Grid>

                    <PaginationStyled count={totalPages} color="primary" onChange={handlePageChange} />
                </Container>
            }
        </Layout>
    )
}

export default Home
