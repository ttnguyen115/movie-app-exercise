import { Pagination } from '@material-ui/lab';
import styled from 'styled-components';

export const PaginationStyled = styled(Pagination)`
    padding: 20px;

    ul {
        justify-content: center;
    }
`;
