import { Grid, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import OrderTable from '../../components/OrderTable';
import Layout from '../../HOCs/Layout';
import { fetchUserByToken } from '../../store/userSlice';
import { CardStyled, ContainerStyled } from './styles';

const UserInfo = () => {
    const dispatch = useDispatch();
    const { user, token } = useSelector(state => state.user);

    useEffect(() => {
        dispatch(fetchUserByToken(token));
    }, [dispatch, token]);

    return (
        <Layout>
            <ContainerStyled maxWidth="md">
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <Typography variant="h6" component="h6">Full Name</Typography>
                        <CardStyled variant="outlined">{user?.hoTen}</CardStyled>
                    </Grid>

                    <Grid item xs={6}>
                        <Typography variant="h6" component="h6">Username</Typography>
                        <CardStyled variant="outlined">{user?.taiKhoan}</CardStyled>
                    </Grid>

                    <Grid item xs={6}>
                        <Typography variant="h6" component="h6">Email</Typography>
                        <CardStyled variant="outlined">{user?.email}</CardStyled>
                    </Grid>

                    <Grid item xs={6}>
                        <Typography variant="h6" component="h6">Phone</Typography>
                        <CardStyled variant="outlined">{user?.soDT}</CardStyled>
                    </Grid>
                </Grid>

                <OrderTable thongTinDatVe={user?.thongTinDatVe} />
            </ContainerStyled>
        </Layout>
    )
}

export default UserInfo
