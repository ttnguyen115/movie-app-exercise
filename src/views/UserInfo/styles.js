import { Card, Container } from "@material-ui/core";
import styled from "styled-components";

export const ContainerStyled = styled(Container)`
    height: calc(100vh - 64px);
    padding: 20px;
`;

export const CardStyled = styled(Card)`
    text-align: center;
    padding: 16px;
`; 