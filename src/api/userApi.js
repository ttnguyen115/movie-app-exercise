import axiosClient from "./axiosClient";

const userApi = {
    login: ({ username, password }) => {
        const url = '/QuanLyNguoiDung/DangNhap';
        return axiosClient.post(url, { "taiKhoan": username, "matKhau": password });
    },

    register: ({ username, password, email, phone, fullname }) => {
        const url = '/QuanLyNguoiDung/DangKy';
        console.log({ username, password, email, phone, fullname })
        const dataValues = {
            "taiKhoan": username,
            "matKhau": password,
            "email": email,
            "soDt": phone,
            "maNhom": "GP01",
            "maLoaiNguoiDung": "KhachHang",
            "hoTen": fullname
        }
        return axiosClient.post(url, dataValues);
    },

    getUserByToken: (token) => {
        const url = '/QuanLyNguoiDung/ThongTinTaiKhoan';
        return axiosClient.post(url, token, {
            headers: {
                Authorization: "Bearer " + token
            }
        });
    },

    refresh: ({ taiKhoan, matKhau }) => {
        const url = '/QuanLyNguoiDung/DangNhap';
        return axiosClient.post(url, { "taiKhoan": taiKhoan, "matKhau": matKhau });
    }
}

export default userApi;