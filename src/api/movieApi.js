import axiosClient from './axiosClient';

const movieApi = {
    getAll: (page) => {
        const url = "/QuanLyPhim/LayDanhSachPhimPhanTrang";
        return axiosClient.get(url, {
            params: {
                maNhom: 'GP03',
                soTrang: page,
                soPhanTuTrenTrang: 20
            }
        });
    },

    get: (id) => {
        const url = '/QuanLyPhim/LayThongTinPhim';
        return axiosClient.get(url, {
            params: {
                "MaPhim": id
            }
        })
    }
}

export default movieApi;