export const data = [
    {
        danhSachGhe: [
            {
                maHeThongRap: "CGV",
                tenHeThongRap: "CGV - CT Plaza",
                maCumRap: "Rạp 3",
                tenCumRap: "Rạp 3",
                maRap: 553,
                tenRap: "Rạp 3",
                maGhe: 63851,
                tenGhe: "131"
            }                                                   
        ],
        maVe: 64510,
        ngayDat: "2021-08-11T23:53:08.877",
        tenPhim: "Raya and the Last Dragon",
        giaVe: 75000,
        thoiLuongPhim: 120
    },
    {
        danhSachGhe: [
            {
                maHeThongRap: "BHDStar",
                tenHeThongRap: "BHD Star Cineplex - Bitexco",
                maCumRap: "Rạp 7",
                tenCumRap: "Rạp 7",
                maRap: 467,
                tenRap: "Rạp 7",
                maGhe: 49967,
                tenGhe: "07"
            }
        ],
        maVe: 65424,
        ngayDat: "2021-08-19T15:11:23.857",
        tenPhim: "Black Window",
        giaVe: 75000,
        thoiLuongPhim: 120
    },
    {
        danhSachGhe: [
            {
                maHeThongRap: "BHDStar",
                tenHeThongRap: "BHD Star Cineplex - Bitexco",
                maCumRap: "Rạp 7",
                tenCumRap: "Rạp 7",
                maRap: 467,
                tenRap: "Rạp 7",
                maGhe: 49987,
                tenGhe: "27"
            }
        ],
        maVe: 65439,
        ngayDat: "2021-08-19T18:57:09.77",
        tenPhim: "Black Window",
        giaVe: 75000,
        thoiLuongPhim: 120
    },
    {
        danhSachGhe: [
            {
                maHeThongRap: "BHDStar",
                tenHeThongRap: "BHD Star Cineplex - Bitexco",
                maCumRap: "Rạp 7",
                tenCumRap: "Rạp 7",
                maRap: 467,
                tenRap: "Rạp 7",
                maGhe: 50012,
                tenGhe: "52"
            }
        ],
        maVe: 65441,
        ngayDat: "2021-08-19T19:08:43.727",
        tenPhim: "Black Window",
        giaVe: 90000,
        thoiLuongPhim: 120
    },
    {
        danhSachGhe: [
            {
                maHeThongRap: "BHDStar",
                tenHeThongRap: "BHD Star Cineplex - Bitexco",
                maCumRap: "Rạp 7",
                tenCumRap: "Rạp 7",
                maRap: 467,
                tenRap: "Rạp 7",
                maGhe: 50021,
                tenGhe: "61"
            }
        ],
        maVe: 65442,
        ngayDat: "2021-08-19T19:08:58.023",
        tenPhim: "Black Window",
        giaVe: 90000,
        thoiLuongPhim: 120
    },
    {
        danhSachGhe: [
            {
                maHeThongRap: "BHDStar",
                tenHeThongRap: "BHD Star Cineplex - Bitexco",
                maCumRap: "Rạp 7",
                tenCumRap: "Rạp 7",
                maRap: 467,
                tenRap: "Rạp 7",
                maGhe: 50011,
                tenGhe: "51"
            }
        ],
        maVe: 65443,
        ngayDat: "2021-08-19T19:10:07.517",
        tenPhim: "Black Window",
        giaVe: 90000,
        thoiLuongPhim: 120
    },
]