import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { PrivateRoute } from "./HOCs/Routes";
import { refreshToken } from "./store/userSlice";
import Details from './views/Details';
import Home from './views/Home';
import NotFound from './views/NotFound';
import UserInfo from "./views/UserInfo";

const App = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(refreshToken()); 
    }, [dispatch]);

    return (
        <Switch>
            <PrivateRoute path="/user/:id" component={UserInfo} />

            <Route path="/movie/:id" component={Details} exact />
            <Route path="/" component={Home} exact />
            
            <Route path="*" component={NotFound} />
        </Switch>
    );
}
  
  export default App;
  