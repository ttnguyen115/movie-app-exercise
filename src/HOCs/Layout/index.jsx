import React from 'react';
import Header from '../../components/Header';
import { LayoutStyled } from './styles';

const Layout = ({ children }) => {
    return (
        <LayoutStyled>
            <Header />
            { children }
        </LayoutStyled>
    )
}

export default Layout;
